#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use]
extern crate rocket;

use rocket::http::{Cookie, Cookies};
use rocket::response::Redirect;
use rocket_contrib::json::JsonValue;
use rocket_contrib::templates::Template;
use rspotify::blocking::client::Spotify;
use rspotify::blocking::oauth2::{SpotifyClientCredentials, SpotifyOAuth};
use rspotify::blocking::util::{generate_random_string, get_token};
use serde::Deserialize;

use std::collections::HashMap;
use std::env;
use std::fs;
use std::path::{Path, PathBuf};

#[derive(Debug, Responder)]
pub enum AppResponse {
    Template(Template),
    Redirect(Redirect),
    Json(JsonValue),
}

#[derive(Deserialize, Debug)]
struct Lyrics {
    result: LyricResult,
}

#[derive(Deserialize, Debug)]
struct LyricResult {
    artist: Artist,
    track: Track,
    probability: f32,
    similarity: f32,
}

#[derive(Deserialize, Debug)]
struct Artist {
    name: String,
}

#[derive(Deserialize, Debug)]
struct Track {
    name: String,
    text: String,
}

const CACHE_PATH: &str = ".spotify_cache/";

fn cache_path(cookies: Cookies) -> PathBuf {
    let project_dir_path = env::current_dir().unwrap();
    let mut cache_path = PathBuf::from(project_dir_path);
    cache_path.push(CACHE_PATH);
    let cache_dir = cache_path.display().to_string();
    cache_path.push(cookies.get("uuid").unwrap().value());
    if !Path::new(cache_dir.as_str()).exists() {
        fs::create_dir_all(cache_dir).unwrap();
    }
    cache_path
}

fn remove_cache_path(mut cookies: Cookies) -> () {
    let project_dir_path = env::current_dir().unwrap();
    let mut cache_path = PathBuf::from(project_dir_path);
    cache_path.push(CACHE_PATH);
    let cache_dir = cache_path.display().to_string();
    if Path::new(cache_dir.as_str()).exists() {
        fs::remove_dir_all(cache_dir).unwrap()
    }
    cookies.remove(Cookie::named("uuid"))
}

fn main() {
    rocket::ignite()
        .mount("/", routes![index, callback, sign_out])
        .attach(Template::fairing())
        .launch();
}

fn auth_manager(cookies: Cookies) -> SpotifyOAuth {
    SpotifyOAuth::default()
        .scope("user-read-currently-playing")
        .client_id("2293252e488642c4a70079cd3c698815")
        .client_secret("119c8eacf5b94946ad57cf67e4797131")
        .redirect_uri("https://lyrics.hmucs.co.uk/callback")
        .cache_path(cache_path(cookies))
        .build()
}

#[get("/")]
fn index(mut cookies: Cookies) -> AppResponse {
    let cookie = cookies.get("uuid");
    if let None = cookie {
        cookies.add(Cookie::new("uuid", generate_random_string(64)));
    }
    let mut auth_manager = auth_manager(cookies);
    let mut context = HashMap::new();
    match auth_manager.get_cached_token() {
        Some(token) => token,
        None => {
            let state = generate_random_string(16);
            let auth_url = auth_manager.get_authorize_url(Some(&state), None);
            context.insert("auth_url", auth_url);
            return AppResponse::Template(Template::render("authorize", context));
        }
    };
    let token_info = get_token(&mut auth_manager).unwrap();
    let client_credential = SpotifyClientCredentials::default()
        .token_info(token_info)
        .build();
    let spotify = Spotify::default()
        .client_credentials_manager(client_credential)
        .build();
    let context = match get_current_song(spotify) {
        Ok((artist, title)) => get_lyric_context(artist, title),
        Err(e) => get_error_context(format!("{:?}", e)),
    };
    AppResponse::Template(Template::render("index", context))
}

#[get("/callback?<code>")]
fn callback(cookies: Cookies, code: String) -> AppResponse {
    let auth_manager = auth_manager(cookies);
    return match auth_manager.get_access_token(code.as_str()) {
        Some(_) => AppResponse::Redirect(Redirect::to("/")),
        _ => {
            let mut context = HashMap::new();
            context.insert("err_msg", "Can not get code!");
            AppResponse::Template(Template::render("error", context))
        }
    };
}

#[get("/sign_out")]
fn sign_out(cookies: Cookies) -> AppResponse {
    remove_cache_path(cookies);
    AppResponse::Redirect(Redirect::to("/"))
}

fn get_current_song(spotify: Spotify) -> Result<(String, String), String> {
    match spotify.current_user_playing_track() {
        Ok(Some(track)) => {
            let track_info = track.item.unwrap();
            Ok((
                track_info
                    .artists
                    .into_iter()
                    .map(|a| a.name)
                    .collect::<Vec<String>>()
                    .join(", "),
                track_info.name,
            ))
        }
        Ok(None) => Err("No track playing.".to_string()),
        Err(e) => Err(format!("{:?}", e)),
    }
}

fn get_error_context(error: String) -> HashMap<&'static str, String> {
    let mut error_context = HashMap::new();
    error_context.insert("artist", error);
    error_context
}

fn get_lyric_context(artist: String, title: String) -> HashMap<&'static str, String> {
    match get_lyrics(&artist, &title) {
        Ok(lyrics) => {
            let mut context = HashMap::new();
            context.insert("title", title);
            context.insert("artist", artist);
            context.insert("lyrics", lyrics);
            context
        }
        Err(e) => get_error_context(format!("{:?}", e)),
    }
}

fn get_lyrics(artist: &String, title: &String) -> Result<String, String> {
    let request_url = format!("https://orion.apiseeds.com/api/music/lyric/{artist}/{track}?apikey=MxNFghaNK9KjYXrH6mKl2rbNhutUxKzNq2gpfGwk0QzEuHHhouSeVUd1n43ybP7x",
        artist = artist,
        track = title);
    let response = reqwest::blocking::get(&request_url).unwrap();
    println!("{:?}", response);
    match response.json::<Lyrics>() {
        Ok(track_result) => Ok(track_result.result.track.text),
        Err(e) => Err("Song lyrics not available.".to_string()),
    }
}
