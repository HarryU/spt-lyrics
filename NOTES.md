# From spotify-tui

async fn get_current_playback(&mut self) {
    let context = self.spotify.current_playback(None, None).await;

    if let Ok(Some(c)) = context {
        let mut app = self.app.lock().await;
        app.current_playback_context = Some(c.clone());
        app.instant_since_last_current_playback_poll = Instant::now();

        if let Some(item) = c.item {
            match item {
                PlayingItem::Track(track) => {
                    if let Some(track_id) = track.id {
                        app.dispatch(IoEvent::CurrentUserSavedTracksContains(vec![track_id]));
                    };
                }
                PlayingItem::Episode(_episode) => {}
            }
        };
    }

    let mut app = self.app.lock().await;
    app.is_fetching_current_playback = false;
}

pub fn get_spotify(token_info: TokenInfo) -> (Spotify, SystemTime) {
    let token_expiry = {
        if let Some(expires_at) = token_info.expires_at {
            SystemTime::UNIX_EPOCH
                + Duration::from_secs(expires_at as u64)
                - Duration::from_secs(10)
        } else {
            SystemTime::now()
        }
    };

    let client_credential = SpotifyClientCredentials::default()
        .token_info(token_info)
        .build();

    let spotify = Spotify::default()
        .client_credentials_manager(client_credential)
        .build();

    (spotify, token_expiry)
}
