# spt-lyrics

Display the lyrics from the currently playing song on Spotify in the browser.

Requires rust nightly:

```
rustup default nightly
cargo run
```

Open [https://lyrics.hmucs.co.uk](https://lyrics.hmucs.co.uk) in the broweser and sign-in to Spotify.
